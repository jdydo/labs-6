﻿using Lab6.MainComponent.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Lab6.ControlPanel.Implementation
{
    /// <summary>
    /// Interaction logic for ConPanel.xaml
    /// </summary>
    partial class ConPanel : Window
    {
        IProgram program;

        public ConPanel(IProgram program)
        {
            InitializeComponent();
            this.program = program;

            //Version verPanel = Assembly.GetExecutingAssembly().GetName().Version;
            //Version verMain = Assembly.GetAssembly(typeof(IProgram)).GetName().Version;

            //if (verMain < verPanel)
            //{
            //    zamkPrzycisk.Visibility = Visibility.Hidden;
            //}
            //else
            //{
            //    zamkPrzycisk.Visibility = Visibility.Visible;
            //}
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            program.Start();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            program.Stop();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            program.OtworzDrzwi();
        }

        //private void Button_Click_4(object sender, RoutedEventArgs e)
        //{
        //    program.zamknijDrzwi();
        //}
    }
}
