﻿using Lab6.ControlPanel.Contract;
using Lab6.MainComponent.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Lab6.ControlPanel.Implementation
{
    public class Posrednik : IControlPanel
    {
        Window window;
        IProgram program;
        
        public Window Window
        {
            get { return window; }
        }

        public Posrednik(IProgram program)
        {
            this.program = program;
            this.window = new ConPanel(program);
        }
    }
}
