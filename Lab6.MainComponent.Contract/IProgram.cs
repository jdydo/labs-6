﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6.MainComponent.Contract
{
    public interface IProgram
    {
        void Start();
        void Stop();
        void OtworzDrzwi();
        //void zamknijDrzwi();
    }
}
