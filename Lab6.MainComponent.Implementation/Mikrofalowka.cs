﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab6.MainComponent.Contract;
using Lab6.Display.Contract;

namespace Lab6.MainComponent.Implementation
{
    public class Mikrofalowka : IProgram
    {
        IDisplay display;

        public Mikrofalowka(IDisplay display)
        {
            this.display = display;
        }
        
        void IProgram.Start()
        {
            display.Text = "START";
        }

        void IProgram.Stop()
        {
            display.Text = "STOP";
        }

        void IProgram.OtworzDrzwi()
        {
            display.Text = "OPEN";
        }

        //void IProgram.zamknijDrzwi()
        //{
        //    display.Text = "CLOSE";
        //}
    }
}
